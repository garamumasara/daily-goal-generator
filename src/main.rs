mod sub;
use sub::*;

fn main() {
    let (file_name, slack_option) = get_arg().unwrap_or_else(|err| error_handle(err));
    run(file_name, slack_option).unwrap_or_else(|err| error_handle(err));

    eprintln!("exit: all things were successfully done");
}

fn run(file_name: OsString, slack_option: SlackOption) -> Rslt<()> {
    let (mut tsv_reader, mut cfg_string, tmp_name) = get_reader(&file_name)?;

    let header = tsv_reader.headers()?.clone();
    let cfg_data = cfg_parse(&mut cfg_string, header.len())?;

    let mut writer = WriterBuilder::new().delimiter(b'\t').from_path(&tmp_name)?;
    writer.write_record(&header)?;

    let record_ptr_key = find_record(&mut tsv_reader, &cfg_data, &header, &mut writer)?;
    let (filled_sentence, type_status) = gen_template(&record_ptr_key, &cfg_data, &header)?;

    if type_status == RzvState::TypedAtLeast1 {
        write_all_record(&filled_sentence, tsv_reader, &mut writer)?;
        eprintln!(
            "the configured data was successfully written to {:?}",
            tmp_name
        );

        delete_file(&file_name)?;
        eprintln!(
            "the original file: {:?} was successfully deleted",
            file_name
        );

        copy_file(&tmp_name.as_os_str(), &file_name)?;
        eprintln!(
            "{:?} was successfully replaced by the temporary file: {:?}",
            file_name, tmp_name
        );
    }
    delete_file(tmp_name.as_os_str())?;
    eprintln!("{:?} was successfully deleted\n", tmp_name);

    let filled = make_filled(filled_sentence, cfg_data);

    match slack_option.is_send() {
        true => send_slack(filled, file_name)?,
        false => println!("{}", filled),
    }

    Ok(())
}
