extern crate const_format;
extern crate csv;
extern crate slack_api;
mod errors;
mod read;
mod write;
use csv::StringRecord;
pub use csv::WriterBuilder;
pub use errors::*;
pub use read::*;
use slack_api::chat::PostMessageError;
pub use slack_api::sync;
pub use std::{ffi::OsString, io, io::Read, iter::FromIterator, num};
pub use write::*;

pub type Rslt<T> = Result<T, Errors>;
pub type StringRec = StringRecord;

pub const CONFIG_EXT: &str = "cfg";
pub const SLACK_EXT: &str = "slack";
pub const TEMPORARY_EXT: &str = "tmp";

pub const YES: &str = "y";
pub const NO: &str = "n";

pub const EMPTY: &str = "#empty";
pub const RESERVED: &str = "#reserve";

#[derive(Debug, Clone)]
pub enum StrField<'a> {
    Word(&'a str),
    Pos(usize),
}

#[derive(Debug)]
pub struct CfgData<'a>(Vec<Vec<StrField<'a>>>);

impl<'a> CfgData<'a> {
    pub fn new() -> Self {
        CfgData(Vec::new())
    }
    pub fn iter(&self) -> std::slice::Iter<'_, Vec<StrField>> {
        self.0.iter()
    }
    pub fn into_iter(self) -> std::vec::IntoIter<Vec<StrField<'a>>> {
        self.0.into_iter()
    }
}

impl<'a> std::ops::Index<usize> for CfgData<'a> {
    type Output = Vec<StrField<'a>>;
    fn index(&self, pos: usize) -> &Self::Output {
        &self.0[pos]
    }
}

impl<'a> std::ops::IndexMut<usize> for CfgData<'a> {
    fn index_mut(&mut self, pos: usize) -> &mut Self::Output {
        &mut self.0[pos]
    }
}

impl<'a> FromIterator<Vec<Vec<StrField<'a>>>> for CfgData<'a> {
    fn from_iter<T: IntoIterator<Item = Vec<Vec<StrField<'a>>>>>(doc: T) -> Self {
        let mut res = CfgData::new();
        doc.into_iter().for_each(|line| {
            res.0.extend(line);
        });
        res
    }
}

#[derive(Debug)]
pub struct SlackOption {
    send: bool,
}

impl SlackOption {
    pub fn is_send(&self) -> bool {
        self.send
    }
    pub fn new() -> Self {
        SlackOption { send: false }
    }
}

#[derive(Debug, PartialEq)]
pub enum RzvState {
    TypedAtLeast1,
    AlreadyFilled,
}

#[derive(Debug)]
pub enum Errors {
    ArgNotFound,
    InvalidOption,
    InputIsEmpty,
    FileOpen(io::ErrorKind, String),
    CsvCrate(csv::ErrorKind),
    StdIO(io::ErrorKind),
    KeyNotFound,
    Cfg(CfgErr),
    Slack(SlackErr),
}

impl From<csv::Error> for Errors {
    fn from(err: csv::Error) -> Self {
        Errors::CsvCrate(err.into_kind())
    }
}

impl From<io::Error> for Errors {
    fn from(err: io::Error) -> Self {
        Errors::StdIO(err.kind())
    }
}

impl From<std::num::ParseIntError> for Errors {
    fn from(err: num::ParseIntError) -> Self {
        Errors::Cfg(CfgErr::IndexParse(err))
    }
}

impl From<PostMessageError<slack_api::requests::Error>> for Errors {
    fn from(err: PostMessageError<slack_api::requests::Error>) -> Self {
        Errors::Slack(SlackErr::Api(err))
    }
}

#[derive(Debug)]
pub enum CfgErr {
    BracketRightNotFound,
    IndexParse(num::ParseIntError),
    IllegalBracketRight,
    IllegalBracketLeft,
    HasInvalidIndex(usize),
    HasNotNumberInBracket(u8),
}

#[derive(Debug)]
pub enum SlackErr {
    ApiKeyNotFound,
    ChannelNotFound,
    Api(PostMessageError<slack_api::requests::Error>),
}

pub fn make_filled(sentence: StringRec, template: CfgData) -> String {
    template
        .into_iter()
        .filter_map(|line| {
            line.into_iter()
                .map(|elem| match elem {
                    StrField::Word(row) => Some(row),
                    StrField::Pos(i) => match &sentence[i] {
                        EMPTY => None,
                        _ => Some(&sentence[i]),
                    },
                })
                .collect::<Option<String>>()
        })
        .map(|s| s + "\n")
        .collect::<String>()
}

pub fn eprint_record(record: &StringRec) {
    eprintln!();
    record.iter().for_each(|field| {
        eprint!("{}", field);
        if field != record.iter().last().unwrap() {
            eprint!(", ");
        }
    })
}

pub fn eprint_template(record: &StringRec, template: &CfgData, header: &StringRec) -> Rslt<()> {
    template
        .iter()
        .filter_map(|line| {
            line.iter()
                .map(|elem| match elem {
                    StrField::Word(s) => Some(s.to_string()),
                    StrField::Pos(i) => match &record[*i] {
                        RESERVED => Some(format!(" {{ NEED TYPING:{} }}", &header[*i])),
                        EMPTY => None,
                        _ => Some(record[*i].to_string()),
                    },
                })
                .collect::<Option<String>>()
        })
        .for_each(|line| eprintln!("{}", line));

    Ok(())
}

pub fn send_slack(filled: String, file_name: OsString) -> Rslt<()> {
    let mut slack_file = open_file(&file_name, SLACK_EXT)?;
    let mut slack_string = String::new();
    slack_file.read_to_string(&mut slack_string)?;
    let mut slack_iter = slack_string.split("\n").collect::<Vec<_>>().into_iter();

    let api_key = slack_iter
        .next()
        .ok_or(Errors::Slack(SlackErr::ApiKeyNotFound))?;
    let channel = slack_iter
        .next()
        .ok_or(Errors::Slack(SlackErr::ChannelNotFound))?;

    let test = sync::chat::PostMessageRequest {
        channel: channel.trim_end(),
        text: filled.trim_end(),
        parse: None,
        link_names: None,
        attachments: None,
        unfurl_links: None,
        unfurl_media: None,
        username: None,
        as_user: Some(true),
        icon_url: None,
        icon_emoji: None,
        thread_ts: None,
        reply_broadcast: None,
    };

    match loop_until_yes_or_no(const_format::formatcp!(
        "You really want to send to slack ({}/{})?",
        YES,
        NO
    ))? {
        true => {
            let client = slack_api::sync::default_client().unwrap();
            let res = slack_api::sync::chat::post_message(&client, api_key.trim_end(), &test)?;

            println!("{:?}", res);
        }
        false => {}
    }

    Ok(())
}
