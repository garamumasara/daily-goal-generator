extern crate const_format;
extern crate csv;
use crate::*;
use csv::{Reader, ReaderBuilder, Writer};
use std::{env, ffi::OsString, fs::File, fs::OpenOptions, io, path::PathBuf};

pub fn get_arg() -> Rslt<(OsString, SlackOption)> {
    let mut arg_iter = env::args_os();
    let file_name = arg_iter.nth(1).ok_or(Errors::ArgNotFound)?;

    let mut slack_option = SlackOption::new();
    slack_option.send = match arg_iter.next() {
        Some(option) => match option.to_str().unwrap() {
            "--slack" | "-s" => Ok(true),
            _ => Err(Errors::InvalidOption),
        },
        None => Ok(false),
    }?;

    Ok((file_name, slack_option))
}

fn cfg_line_parse_outside(line: &str, header_size: usize) -> Rslt<(Vec<StrField>, usize)> {
    let mut res = Vec::new();
    let (mut start, mut index) = (0, 0);
    while index < line.len() {
        let byte = line.as_bytes()[index];
        match byte {
            b'{' => {
                if start < index {
                    res.push(StrField::Word(&line[start..index]));
                }
                let (field_pos, offset) = cfg_line_parse_inside(&line[index + 1..], header_size)?;
                res.push(field_pos);
                index += offset;
                start = index;
            }
            b'}' => return Err(Errors::Cfg(CfgErr::IllegalBracketRight)),
            _ => index += 1,
        }
    }
    if start < line.len() {
        res.push(StrField::Word(&line[start..]));
    }
    Ok((res, line.len()))
}

fn cfg_line_parse_inside(line: &str, header_size: usize) -> Rslt<(StrField, usize)> {
    for (index, byte) in line.as_bytes().iter().enumerate() {
        match byte {
            b'0'..=b'9' => continue,
            b'{' => return Err(Errors::Cfg(CfgErr::IllegalBracketLeft)),
            b'}' => {
                let result = cfg_parse_index(&line[..index])?;
                match result == 0 || result > header_size {
                    true => return Err(Errors::Cfg(CfgErr::HasInvalidIndex(result))),
                    false => return Ok((StrField::Pos(result - 1), index + 2)),
                }
            }
            _ => return Err(Errors::Cfg(CfgErr::HasNotNumberInBracket(*byte))),
        }
    }
    Err(Errors::Cfg(CfgErr::BracketRightNotFound))
}

fn cfg_parse_index(index: &str) -> Rslt<usize> {
    index
        .parse::<usize>()
        .or_else(|e| Err(Errors::Cfg(CfgErr::IndexParse(e))))
}

pub fn cfg_parse(cfg_reader: &String, header_size: usize) -> Rslt<CfgData> {
    let cfg_vec = cfg_reader.split_terminator('\n').collect::<Vec<_>>();
    cfg_vec
        .into_iter()
        .map(|line| {
            let mut res = Vec::new();
            let mut index = 0;
            while index < line.len() {
                let byte = line.as_bytes()[index];
                match byte {
                    b'}' => return Err(Errors::Cfg(CfgErr::IllegalBracketRight)),
                    _ => {
                        let (result, offset) = cfg_line_parse_outside(&line[index..], header_size)?;
                        index += offset;
                        res.push(result);
                    }
                }
                index += 1;
            }
            Ok(res)
        })
        .collect::<Rslt<_>>()
}

pub fn find_record(
    tsv_reader: &mut Reader<File>,
    template: &CfgData,
    header: &StringRec,
    write_buf: &mut Writer<File>,
) -> Rslt<StringRec> {
    loop {
        let key_at_field1 = get_key(header.get(0))?;
        let record_ptr_key = get_record(&key_at_field1, tsv_reader, write_buf)?;

        eprint_record(header);
        eprint_record(&record_ptr_key);
        eprintln!("\n");
        eprint_template(&record_ptr_key, template, header)?;

        if loop_until_yes_or_no(const_format::formatcp!(
            "Did you choose this ({}/{})?",
            YES,
            NO
        ))? {
            return Ok(record_ptr_key);
        }
    }
}

pub fn gen_template(
    rec_ptr_key: &StringRec,
    template: &CfgData,
    header: &StringRec,
) -> Rslt<(StringRec, RzvState)> {
    loop {
        let (result, type_status) = make_reserve_typed(rec_ptr_key, header)?;

        if type_status == RzvState::AlreadyFilled {
            return Ok((result, type_status));
        }
        eprint_template(&result, &template, header)?;
        if loop_until_yes_or_no(const_format::formatcp!(
            "Did you typed the value correctly ({}/{})?",
            YES,
            NO
        ))? {
            return Ok((result, type_status));
        }
    }
}

pub fn open_file(src_name: &OsString, extension: &str) -> Rslt<File> {
    let dest_name = get_name(src_name, extension);
    let dest_file = OpenOptions::new()
        .read(true)
        .open(dest_name)
        .or_else(|err| Err(Errors::FileOpen(err.kind(), extension.to_string())))?;

    Ok(dest_file)
}

fn get_name(src_name: &OsString, extension: &str) -> PathBuf {
    let mut dest_name = PathBuf::new();
    dest_name.push(src_name);
    dest_name.as_path().file_stem();
    dest_name.set_extension(extension);

    dest_name
}

pub fn get_reader(file_name: &OsString) -> Rslt<(Reader<File>, String, PathBuf)> {
    let tsv_reader = ReaderBuilder::new()
        .delimiter(b'\t')
        .has_headers(true)
        .quoting(false)
        .from_path(&file_name)?;

    let mut cfg_file = open_file(&file_name, CONFIG_EXT)?;
    let mut cfg_string = String::new();
    cfg_file.read_to_string(&mut cfg_string)?;

    let tmp_name = get_name(&file_name, TEMPORARY_EXT);

    Ok((tsv_reader, cfg_string, tmp_name))
}

fn get_key(field1: Option<&str>) -> Rslt<String> {
    let field1 = field1.ok_or_else(|| Errors::InputIsEmpty)?;
    let key = print_prompt_and_get_line(&format!("Please enter a value of field1 ({})", field1))?;

    Ok(key)
}

fn get_stdin_line() -> Rslt<String> {
    let mut line = String::new();
    io::stdin().read_line(&mut line)?;
    remove_cr_lf(&mut line);

    Ok(line)
}

fn remove_cr_lf(line: &mut String) {
    (0..2).for_each(|_| match line.as_bytes().last() {
        Some(13) | Some(10) => {
            line.pop();
        }
        _ => {}
    });
}

fn get_record(key: &str, rdr: &mut Reader<File>, wtr: &mut Writer<File>) -> Rslt<StringRec> {
    for result in rdr.records() {
        let record = result?;
        match &record[0] {
            n if n == key => return Ok(record),
            _ => wtr.write_record(&record)?,
        }
    }

    Err(Errors::KeyNotFound)
}

fn make_reserve_typed(record: &StringRec, header: &StringRec) -> Rslt<(StringRec, RzvState)> {
    let mut type_status = RzvState::AlreadyFilled;
    let res = header
        .iter()
        .zip(record.iter())
        .map(|(name, now_field)| match now_field {
            RESERVED => {
                let new_field = print_prompt_and_get_line(&format!("{}?", name))?;
                type_status = RzvState::TypedAtLeast1;
                Ok(new_field)
            }
            _ => Ok(now_field.to_string()),
        })
        .collect::<Rslt<StringRec>>()?;
    eprintln!();

    Ok((res, type_status))
}

pub fn loop_until_yes_or_no(description: &str) -> Rslt<bool> {
    loop {
        let key = print_prompt_and_get_line(description)?;

        if let Some(typed) = yes_no_or_else(&key) {
            return Ok(typed);
        }
    }
}

fn print_prompt_and_get_line(description: &str) -> Rslt<String> {
    eprintln!("{}", description);
    eprint!("> ");
    get_stdin_line()
}

fn yes_no_or_else(y_or_n: &str) -> Option<bool> {
    match y_or_n {
        YES => Some(true),
        NO => Some(false),
        _ => None,
    }
}
