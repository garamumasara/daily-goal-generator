extern crate csv;
use crate::*;
use csv::{Reader, Writer};
use std::{
    ffi::OsStr,
    fs::{self, File},
};
type Fl = File;

pub fn write_all_record(filled: &StringRec, mut rdr: Reader<Fl>, wtr: &mut Writer<Fl>) -> Rslt<()> {
    wtr.write_record(filled)?;
    for result in rdr.records() {
        let record = result?;
        wtr.write_record(&record)?;
    }
    wtr.flush()?;
    Ok(())
}

pub fn delete_file(file_name: &OsStr) -> Rslt<()> {
    fs::remove_file(file_name)?;
    Ok(())
}

pub fn copy_file(src_name: &OsStr, dst_name: &OsStr) -> Rslt<()> {
    fs::copy(src_name, dst_name)?;
    Ok(())
}
