use crate::*;
use csv;
use std::io;

pub fn error_handle(error: Errors) -> ! {
    use Errors::*;
    match error {
        ArgNotFound => {
            eprintln!("error: argument not found");
            eprintln!("usage: program <tsv_file>");
        }
        StdIO(io_err) => {
            err_std_io(io_err);
        }
        FileOpen(kind, ext) => {
            err_open(kind, &ext);
        }
        InputIsEmpty => {
            eprintln!("the input file is empty");
        }
        KeyNotFound => {
            eprintln!("the key you typed was not found");
        }
        Cfg(cfg_err) => {
            err_cfg(cfg_err);
        }
        CsvCrate(csv_err) => {
            err_csv_crate(csv_err);
        }
        Slack(slk_err) => {
            err_slack(slk_err);
        }
        err => eprintln!("an error occured: {:?}", err),
    }
    eprintln!("error: aborted");
    std::process::exit(1);
}

fn err_open(kind: io::ErrorKind, ext: &str) {
    use std::io::ErrorKind::*;
    if kind == NotFound {
        eprintln!(".{} file was not found", ext);
        match ext {
            CONFIG_EXT => {
                eprintln!("note: you must create a file that has the same name and path as the input file you entered with .{} extension", CONFIG_EXT);
            }
            SLACK_EXT => {
                eprintln!("note: you must create a file that has the same name and path as the input file you entered with .{} extension", SLACK_EXT);
            }
            _ => {}
        }
    }
}

fn err_csv_crate(err: csv::ErrorKind) {
    use csv::ErrorKind::*;
    match err {
        Io(err) => match err.kind() {
            io::ErrorKind::NotFound => {
                eprintln!("input file was not found");
            }
            _ => {
                eprintln!("tsv_io_error occured: {:?}", err);
            }
        },
        _ => {
            eprintln!("tsv error occured: {:?}", err);
        }
    }
}

fn err_std_io(err: io::ErrorKind) {
    eprintln!("std io error occured: {:?}", err);
}

fn err_cfg(err: CfgErr) {
    use CfgErr::*;
    match err {
        BracketRightNotFound => {
            eprintln!(
                "the .{} file has '{{' but not found '}}' in the same line",
                CONFIG_EXT
            );
        }
        IndexParse(parse_err) => {
            // temporary
            eprintln!(".{} bracket ParseError: {}", CONFIG_EXT, parse_err);
        }
        HasNotNumberInBracket(byte) => {
            match byte.is_ascii() {
                true => {
                    eprintln!(
                        "not a number found in .{} bracket: {}, hex: 0x{:x?}",
                        CONFIG_EXT, byte as char, byte
                    );
                }
                false => {
                    eprintln!(
                        "not a number found in .{} bracket: hex: {:x?}",
                        CONFIG_EXT, byte
                    );
                }
            }
            eprintln!("note: the bracket, \"{{}}\" in .{} can only contain positive decimal numbers that is limited up to the number of field", CONFIG_EXT);
        }
        HasInvalidIndex(index) => {
            eprintln!(
                "there is an index that is greater than the size of the header in .{}{{size: {}}}",
                CONFIG_EXT, index
            );
        }
        _ => eprintln!(".{} error occured: {:?}", CONFIG_EXT, err),
    }
}

fn err_slack(err: SlackErr) {
    use SlackErr::*;
    match err {
        ApiKeyNotFound => {
            eprintln!("slack api key not found in .{} file", SLACK_EXT);
        }
        ChannelNotFound => {
            eprintln!("slack channel not found in .{} file", SLACK_EXT);
        }
        Api(err) => {
            eprintln!("slack api error: {:?}", err);
        } // _ => {
          //     eprintln!("slack file error: {:?}", err);
          // }
    }
}
